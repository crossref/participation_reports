import { test, expect } from '@playwright/test';
import membersData from '../e2e/mocks/apps-data-op-members.json';
import participationSummaryForMember12Data from '../e2e/mocks/apps-data-op-participation-summary-member-12.json';
import participationSummaryForMember5401Data from '../e2e/mocks/apps-data-op-participation-summary-member-5401.json';
import apiMembers12Data from '../e2e/mocks/api-members-12.json';
import apiMembers5401Data from '../e2e/mocks/api-members-5401.json';
import apiJournal20603274Data from '../e2e/mocks/api-journal-20603274.json';
import participationSummaryForMember12PubyearCurrentData from '../e2e/mocks/apps-data-op-participation-summary-member-12-pubyear-current.json';
import participationSummaryForMember5401PubyearCurrentData from '../e2e/mocks/apps-data-op-participation-summary-member-5401-pubyear-current.json';
import participationSummaryForMember5401PubyearCurrentPubid20603274Data from '../e2e/mocks/apps-data-op-participation-summary-member-5401-pubyear-current-pubid-20603274.json';
import journalArticlePublicationsForMember5401Data from '../e2e/mocks/apps-data-op-publications-member-5401-contenttype-journal-articles.json';

/**
 * Gets the percentage value for a given metric by title.
 * @param {Page} page - The Playwright page object.
 * @param {string} title - The title of the metric.
 * @returns {Promise<string>} - The percentage text.
 */
async function getPercentageForTitle(page, title) {
    // Find the element with the given title and traverse to the corresponding percentage.
    const percentage = await page.locator(`.check:has(.title:has-text("${title}")) .percent`).textContent();
    return percentage.trim();
}

function mockMembers(route) {
    route.fulfill({
      status: 200,
      contentType: 'application/json',
      body: JSON.stringify(membersData)
    });
}

function mockParticipationSummaryForMember12(route) {
    route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify(participationSummaryForMember12Data)
      });
}

function mockParticipationSummaryForMember5401(route) {
    route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify(participationSummaryForMember5401Data)
      });
}

function mockApiMember12(route) {
    route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify(apiMembers12Data)
      });
}

function mockApiMember5401(route) {
    route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify(apiMembers5401Data)
      });
}

function mockApiJournal20603274(route) {
    route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify(apiJournal20603274Data)
      });
}

function mockParticipationSummaryForMember5401PubyearCurrent(route) {
    route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify(participationSummaryForMember5401PubyearCurrentData)
      });
}

function mockParticipationSummaryForMember5401PubyearCurrentPubid20603274(route) {
    route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify(participationSummaryForMember5401PubyearCurrentPubid20603274Data)
      });
}

function mockJournalArticlePublicationsForMember5401(route) {
    route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify(journalArticlePublicationsForMember5401Data)
      });
}
  
function mockParticipationSummaryForMember12PubyearCurrent(route) {
    route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify(participationSummaryForMember12PubyearCurrentData)
      });
}

  
test.describe('Dashboard metrics tests', () => {
    test.beforeEach(async ({ page }) => {
        await page.route('https://apps.crossref.org/prep/data?op=members', mockMembers);
        await page.route('https://apps.crossref.org/prep/data?op=participation-summary&memberid=12', mockParticipationSummaryForMember12)
        await page.route('https://apps.crossref.org/prep/data?op=participation-summary&memberid=5401', mockParticipationSummaryForMember5401)
        await page.route('https://apps.crossref.org/prep/data?op=participation-summary&memberid=12&pubyear=current', mockParticipationSummaryForMember12PubyearCurrent)
        await page.route('https://apps.crossref.org/prep/data?op=participation-summary&memberid=5401&pubyear=current', mockParticipationSummaryForMember5401PubyearCurrent)
        await page.route('https://apps.crossref.org/prep/data?op=participation-summary&memberid=5401&pubyear=current&pubid=20603274', mockParticipationSummaryForMember5401PubyearCurrentPubid20603274)
        await page.route('https://apps.crossref.org/prep/data?op=publications&memberid=5401&contenttype=Journal%20articles', mockJournalArticlePublicationsForMember5401)
        await page.route('https://api.crossref.org/members/12?mailto=support@crossref.org', mockApiMember12)
        await page.route('https://api.crossref.org/members/5401?mailto=support@crossref.org', mockApiMember5401)
        await page.route('https://api.crossref.org/journals/20603274?mailto=support@crossref.org', mockApiJournal20603274)
        // Intercept the request for the matomo.js script and respond with a no-op
        await page.route('https://crossref.matomo.cloud/matomo.js', (route) => {
            route.fulfill({
            status: 200,
            contentType: 'application/javascript',
            body: '', // No-op or empty response
            });
        });
    });

    test('should display base metrics correcrtly', async ({ page }) => {
        await page.goto('./12');
        // Check metrics
        const referencesPcnt = await getPercentageForTitle(page, 'References')
        const rorIDsPcnt = await getPercentageForTitle(page, 'ROR IDs')
        await expect(referencesPcnt).toBe('8%');
        await expect(rorIDsPcnt).toBe('99%');
        // Filter by reports
        await page.locator('.filterArrow').first().click();
        await page.locator('#Reports_button').click();
        // Check metrics
        const ORCIDIDsPcnt = await getPercentageForTitle(page, 'ORCID IDs')
        await expect(ORCIDIDsPcnt).toBe('6%');
    });
    
    test('should display bolt-on metrics ROR IDs and Affiliations correctly', async ({ page }) => {
        await page.goto('./5401')
        // Check metrics
        await expect(await getPercentageForTitle(page, 'References')).toBe('76%')
        // Check extended metrics retrieved from the REST Api
        const rorIDsPcnt = await getPercentageForTitle(page, 'ROR IDs');
        const affiliationsPcnt = await getPercentageForTitle(page, 'Affiliations');
        await expect(rorIDsPcnt).toBe('28%')
        await expect(affiliationsPcnt).toBe('92%')
        // Filter by journal
        const searchByTitleBox = await page.getByRole('textbox')
        await searchByTitleBox.fill('j')
        await page.getByText('Journal of Environmental').click()
        // Wait for load (should be instant as it's mocked)
        await expect(page.locator('.loadWhiteScreen')).not.toBeVisible()
        // Check metrics
        const referencesPcnt = await getPercentageForTitle(page, 'References')
        const abstractsPcnt = await getPercentageForTitle(page, 'Abstracts')
        const rorIDsForPubnPcnt = await getPercentageForTitle(page, 'ROR IDs')
        await expect(referencesPcnt).toBe('92%')
        await expect(abstractsPcnt).toBe('100%')
        await expect(rorIDsForPubnPcnt).toBe('100%')
        // Clear the journal filter
        await page.locator('.titleFilterX').first().click()
        // Check metrics
        await expect(await searchByTitleBox.inputValue()).toBe('')
        await expect(await getPercentageForTitle(page, 'ROR IDs')).toBe('28%')
        await expect(await getPercentageForTitle(page, 'References')).toBe('76%')
    });
}); 